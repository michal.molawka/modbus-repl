pub mod args;
pub mod help;
pub mod modbus;
pub mod print;
pub mod connection;
pub mod errors;
pub mod context;
pub mod commands;
pub mod repl;

use connection::*;
use context::*;
use commands::*;
use errors::*;
use std::io::Write;
use std::process;
use std::net::{TcpStream, UdpSocket};
use args::*;
use help::*;

fn main() {
	let args = Args::new();
	check_help_flag(&args);
	validate_comm_protocol(&args);
	let mut ctx = init_context(&args);
	repl(&mut ctx);
}

fn repl(ctx: &mut Context) {
	let mut input = String::new();
	loop {
		eprint!("{}://{} > ", ctx.protocol, ctx.addr);
		input.clear();
		std::io::stdout().flush().unwrap();
		std::io::stdin().read_line(&mut input).expect("Failed to read line");

		if is_exit_command(input.trim()) {
			break;
		}

		input = sanitize_command(input.as_str());

		match eval(&input, ctx) {
			Ok(_result) => eprintln!("Ok"),
			Err(e) => eprintln!("{e}"),
		}
	}
}

fn eval(cmd: &str, ctx: &mut Context) -> Result<ModbusResult, String> {
	let parts: Vec<&str> = cmd.trim().split(" ").collect();
	let parts_slice = parts.as_slice();

	match parts_slice.get(0).unwrap_or(&"unknown") {
		s if s == &"rc" => read_coils(parts_slice, ctx),
		s if s == &"rd" => read_discrete(parts_slice, ctx),
		s if s == &"ri" => read_inputs(parts_slice, ctx),
		s if s == &"rh" => read_holding(parts_slice, ctx),
		s if s == &"wc" => write_coils(parts_slice, ctx),
		s if s == &"WC" => write_single_coil(parts_slice, ctx),
		s if s == &"wh" => write_holding_registers(parts_slice, ctx),
		s if s == &"WH" => write_single_holding_register(parts_slice, ctx),
		s if s == &"sleep" => sleep(parts_slice),
		s if s == &"help" => print_help(),
		_ => Err("Unknown command".to_string()),
	}
}


fn is_exit_command(cmd: &str) -> bool {
	cmd == "q" || cmd == "quit" || cmd == "exit"
}

fn validate_comm_protocol(args: &Args) {
	let protocols = [args.tcp.is_some(), args.udp.is_some(), args.rtu.is_some()];
	let proto_cnt = protocols.iter().filter(|&val| *val).count();

	if proto_cnt == 0 {
		eprintln!("Error: No communication protocol specified");
		let _ = print_help();
		process::exit(1);
	}

	if proto_cnt > 1 {
		eprintln!("Error: Multiple communication protocols specified");
		process::exit(1);
	}
}

fn sanitize_command(input: &str) -> String {
    let mut result = String::new();
    let mut prev_char: Option<char> = None;

    for c in input.trim().chars() {
        if c != ' ' || prev_char != Some(' ') {
            result.push(c);
        }
        prev_char = Some(c);
    }

    result
}

fn init_context(args: &Args) -> Context {
	if args.rtu.is_some() {
		eprintln!("{}", fmt_error(ERR_TYPE_NOT_IMPL, "RTU is not supported yet"));
		process::exit(1);
	}

	if args.udp.is_some() {
		Context {
			protocol: "UDP",
			connection: Box::new(UdpSocket::bind("0.0.0.0:0").unwrap()),
			addr: &args.udp.as_ref().unwrap(),
			fmt: args.fmt,
		}
	} else {
		Context {
			protocol: "TCP",
			connection: Box::new(TcpStream::connect(args.tcp.clone().unwrap()).unwrap()),
			addr: &args.tcp.as_ref().unwrap(),
			fmt: args.fmt,
		}
	}
}

