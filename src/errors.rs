

pub const ERR_TYPE_INPUT: &str = "InputError";
pub const ERR_TYPE_NOT_IMPL: &str = "NotImplementedError";

pub fn fmt_error(typ: &str, msg: &str) -> String {
	format!("{typ}: {msg}")
}

pub fn mk_error<T>(typ: &str, msg: &str) -> Result<T, String> {
	Err(fmt_error(typ, msg))
}

pub fn mk_input_error<T>(msg: &str) -> Result<T, String> {
	mk_error(ERR_TYPE_INPUT, msg)
}
