
// const MODBUS_FN_CODE_WRITE_SINGLE_COIL:u8 = 5;
const MODBUS_FN_CODE_WRITE_MULTIPLE_COILS: u8 = 15;
// const MODBUS_FN_CODE_WRITE_SINGLE_HOLDING_REGISTER: u8 = 6;
const MODBUS_FN_CODE_WRITE_MULTIPLE_HOLDING_REGISTERS: u8 = 16;


pub const MODBUS_FN_CODE_READ_COILS: u8 = 1;
pub const MODBUS_FN_CODE_READ_DISCRETE_INPUTS: u8 = 2;
pub const MODBUS_FN_CODE_READ_MULTIPLE_HOLDING_REGISTERS: u8 = 3;
pub const MODBUS_FN_CODE_READ_INPUT_REGISTERS: u8 = 4;

fn create_modbus_write_multiple_registers(start_address: u16, data: &[u16]) -> Vec<u8> {
	let mut buffer: Vec<u8> = vec![];

	buffer.push(MODBUS_FN_CODE_WRITE_MULTIPLE_HOLDING_REGISTERS);
	// Start Address (2 bytes): Address of the first register to be written
	buffer.extend_from_slice(&start_address.to_be_bytes());
	// Number of registers (2 bytes): Number of registers to be written
	let num_registers = data.len() as u16;
	let num_bytes = (num_registers * 2) as u8;
	buffer.extend_from_slice(&num_registers.to_be_bytes());
	buffer.extend_from_slice(&[num_bytes]);

	for &word in data {
		buffer.extend_from_slice(&word.to_be_bytes());
	}

	buffer
}

pub fn create_modbus_read_msg(fn_code: u8, start_address: u16, quantity: u16) -> Vec<u8> {
	let mut buffer: Vec<u8> = vec![fn_code];

	// Starting Address (2 bytes)
	buffer.extend_from_slice(&[(start_address >> 8) as u8, (start_address & 0xFF) as u8]);

	// Quantity of Input Registers (2 bytes)
	buffer.extend_from_slice(&[(quantity >> 8) as u8, (quantity & 0xFF) as u8]);

	buffer
}

pub fn create_modbus_tcp_read_msg(transaction_id: u16, unit_id: u8, fn_code: u8, start_address: u16, quantity: u16) -> Vec<u8> {
	// Modbus TCP header: Transaction Identifier (2 bytes)
	let mut buffer = vec![
		(transaction_id >> 8) as u8, (transaction_id & 0xFF) as u8,
		0, 0 // Protocol Identifier
	];
	// Length (2 bytes): Message length excluding header
	// Message length is 6 (excluding header) + 2 (for unit identifier and function code)
	buffer.extend_from_slice(&[((6 + 2) >> 8) as u8, ((6 + 2) & 0xFF) as u8]);
	// Unit Identifier (1 byte): Modbus unit id
	buffer.push(unit_id);

	let mb_msg_buffer = create_modbus_read_msg(fn_code, start_address, quantity);
	buffer.extend(&mb_msg_buffer);

	buffer
}

pub fn create_modbus_tcp_write_registers_msg(transaction_id: u16, unit_id: u8, start_address: u16, data: &[u16]) -> Vec<u8> {
	let mut buffer = vec![
		(transaction_id >> 8) as u8, (transaction_id & 0xFF) as u8,
		0, 0, // Protocol Identifier
	];

	// Length (2 bytes): Length of the remaining message (PDU)
	let pdu_length = 7 + 2 * data.len();
	buffer.extend_from_slice(&(pdu_length as u16).to_be_bytes());
	// Unit Identifier (1 byte): Identifies the slave device
	buffer.push(unit_id);

	buffer.extend_from_slice(&create_modbus_write_multiple_registers(start_address, data));

	buffer
}

pub fn create_modbus_tcp_write_coils_msg(transaction_id: u16, unit_id: u8, start_address: u16, bits: &[bool]) -> Vec<u8> {
	// Modbus TCP header: Transaction Identifier (2 bytes)
	let mut buffer = vec![(transaction_id >> 8) as u8, (transaction_id & 0xFF) as u8];
	// Protocol Identifier (2 bytes): Always 0 for Modbus TCP
	buffer.extend_from_slice(&[0, 0]);
	// Length (2 bytes): Length of the remaining message (PDU)
	let byte_cnt = (bits.len() / 8) + (if bits.len() % 8 == 0 { 0 } else { 1 });
	let pdu_length = 2 + 2 + 2 + 1 + 2 + byte_cnt;
	buffer.extend_from_slice(&(pdu_length as u16).to_be_bytes());
	// Unit Identifier (1 byte): Identifies the slave device
	buffer.push(unit_id);
	// Function Code (1 byte): Specifies the function of the request
	buffer.push(MODBUS_FN_CODE_WRITE_MULTIPLE_COILS);
	// Start Address (2 bytes): Address of the first coil to be written
	buffer.extend_from_slice(&start_address.to_be_bytes());
	// Number of Coils (2 bytes): Number of coils to be written
	let num_coils = bits.len() as u16;
	buffer.extend_from_slice(&num_coils.to_be_bytes());
	// Byte Count (1 byte): Number of bytes in the data field
	buffer.push(byte_cnt as u8);

	let mut data = vec![0; byte_cnt];

	for (idx, &val) in bits.iter().enumerate() {
		let byte_idx = idx / 8;
		let bit_idx = idx % 8;

		let byte = data.get_mut(byte_idx).unwrap();
		if val {
			*byte = *byte | (1 << bit_idx) as u8;
		} else {
			*byte = *byte & !(1 << bit_idx) as u8;
		}
	}

	// Coils Data (N bytes): Data representing the state of coils
	buffer.extend_from_slice(&data);
	buffer
}
