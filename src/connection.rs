use std::net::{TcpStream, UdpSocket};
use std::io::{Read, Write};

pub type ModbusResult = ();
pub type SendResult = ();
pub type ReceiveResult = usize;

pub trait Connection {
	fn send(&mut self, addr: &str, data: &[u8]) -> SendResult;
	fn receive(&mut self, buffer: &mut [u8]) -> ReceiveResult;
}

impl Connection for UdpSocket {
	fn send(&mut self, addr: &str,  data: &[u8]) -> SendResult {
		self.send_to(&data, addr).unwrap();
	}

	fn receive(&mut self, buffer: &mut [u8]) -> ReceiveResult {
		let (bytes_read, _addr) = self.recv_from(buffer).unwrap();
		bytes_read
	}
}

impl Connection for TcpStream {
	fn send(&mut self, _addr: &str,  data: &[u8]) -> SendResult {
		self.write_all(&data).unwrap();
	}

	fn receive(&mut self, buffer: &mut [u8]) -> ReceiveResult {
		let bytes_read = self.read(buffer).unwrap();
		bytes_read
	}
}