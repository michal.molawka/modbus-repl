use crate::connection::*;
use crate::args::OutputFormat;

pub struct Context<'a> {
	pub protocol: &'static str,
	pub connection: Box<dyn Connection>,
	pub addr: &'a str,
	pub fmt: OutputFormat,
}
