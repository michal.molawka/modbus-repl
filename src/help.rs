use std::process;
use crate::args::Args;

pub fn print_help() -> Result<(), String>
{
	eprintln!("modbus-repl help:\n");
	eprintln!("  --udp ADDR                create ModbusUDP socket sending data to given address\n");
	eprintln!("  --tcp ADDR                connect to ModbusTCP server at given address\n");
	eprintln!("  --rtu ADDR                connect to ModbusRTU server at given address\n");
	eprintln!("  --fmt FMT                 output format, supported: json, yml, columns\n");
	eprintln!("  --script PATH             run a script from a file at given path\n");
	eprintln!("  --exec COMMAND            execute given command and exit\n");

	Ok(())
}

pub fn check_help_flag(args: &Args) {
	if args.help.is_some() {
		let _ = print_help();
		process::exit(0);
	}
}
