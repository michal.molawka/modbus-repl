use std::env;

#[derive(Debug, Clone, Copy)]
pub enum OutputFormat {
	Columns,
	Json,
	Yml,
	List,
	HexList,
}

fn output_format_from_str(fmt: &str) -> OutputFormat {
	match fmt.to_lowercase().as_str() {
		"json" => OutputFormat::Json,
		"yml" => OutputFormat::Yml,
		"list" => OutputFormat::List,
		"hex_list" => OutputFormat::HexList,
		_ => OutputFormat::Columns,
	}
}

#[derive(Debug)]
pub struct Args {
	pub help: Option<String>,
	pub tcp: Option<String>,
	pub udp: Option<String>,
	pub rtu: Option<String>,
	pub fmt: OutputFormat,
	pub script: Option<String>,
	pub exec: Option<String>,
}

fn find_arg(arg_list: &Vec<String>, name: &str) -> Option<String> {
	let mut iter = arg_list.iter().enumerate().peekable();

	while let Some((_idx, arg)) = iter.next() {
		if !arg.starts_with(name) {
			continue;
		}

		if let Some(idx_eq) = arg.find('=') {
			return Some(arg[idx_eq + 1..].to_string());
		}

		if let Some(next_arg) = iter.peek() {
			return Some(next_arg.1.to_string());
		}

		return Some("".to_string());
	}

	None
}

impl Args {
	pub fn new() -> Self {
		let arg_list: Vec<String> = env::args().skip(1).collect();

		Args {
			help: find_arg(&arg_list, "--help"),
			tcp: find_arg(&arg_list, "--tcp"),
			udp: find_arg(&arg_list, "--udp"),
			rtu: find_arg(&arg_list, "--rtu"),
			fmt: output_format_from_str(&find_arg(&arg_list, "--fmt").unwrap_or_default()),
			script: find_arg(&arg_list, "--script"),
			exec: find_arg(&arg_list, "--exec"),
		}
	}
}
