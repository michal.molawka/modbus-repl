use crate::args::OutputFormat;

pub fn format_registers(fmt: &OutputFormat, data: &[u8], start_addr: u16, cnt: u16) -> String {
	match fmt {
		OutputFormat::Columns => format_registers_columns(data, start_addr, cnt),
		OutputFormat::Json =>  format_registers_json(data, start_addr, cnt),
		OutputFormat::Yml =>  format_registers_yml(data, start_addr, cnt),
		OutputFormat::List =>  format_registers_list(data, start_addr, cnt),
		OutputFormat::HexList => format_registers_hexlist(data, start_addr, cnt),
	}
}

pub fn format_coils(fmt: &OutputFormat, data: &[u8], start_addr: u16, cnt: u16) -> String {
	match fmt {
		OutputFormat::Columns => format_coils_columns(data, start_addr, cnt),
		OutputFormat::Json =>  format_coils_json(data, start_addr, cnt),
		OutputFormat::Yml =>  format_coils_yml(data, start_addr, cnt),
		OutputFormat::List =>  format_coils_list(data, start_addr, cnt),
		OutputFormat::HexList => format_coils_hexlist(data, start_addr, cnt),
	}
}

fn format_coils_columns(data: &[u8], start_addr: u16, cnt: u16) -> String {
	let mut lines: Vec<String> = vec!["  addr | value".to_string()];

	for idx in 0..cnt {
		let address = start_addr + idx;
		let byte_idx = idx / 8;
		let bit_idx = idx % 8;

		let byte = data.get(byte_idx as usize).unwrap_or(&0);
		let coil = match byte & (1 << bit_idx) {
			0 => 0,
			_ => 1,
		};

		lines.push(format!("{:6}: {}", address, coil));
	}

	lines.join("\n")
}

fn format_coils_json(data: &[u8], start_addr: u16, cnt: u16) -> String {
	let mut lines: Vec<String> = Vec::new();

	for idx in 0..cnt {
		let address = start_addr + idx;
		let byte_idx = idx / 8;
		let bit_idx = idx % 8;

		let byte = data.get(byte_idx as usize).unwrap_or(&0);
		let coil = match byte & (1 << bit_idx) {
			0 => 0,
			_ => 1,
		};

		lines.push(format!("{{ \"address\": {}, \"value\": {} }}", address, coil));
	}

	format!("[ {} ]", lines.join(", "))
}

fn format_coils_yml(data: &[u8], start_addr: u16, cnt: u16) -> String {
	let mut lines: Vec<String> = Vec::new();

	for idx in 0..cnt {
		let address = start_addr + idx;
		let byte_idx = idx / 8;
		let bit_idx = idx % 8;

		let byte = data.get(byte_idx as usize).unwrap_or(&0);
		let coil = match byte & (1 << bit_idx) {
			0 => 0,
			_ => 1,
		};

		lines.push(format!("- address: {}\n  value: {}", address, coil));
	}

	lines.join("\n")
}

fn format_coils_list(data: &[u8], _start_addr: u16, cnt: u16) -> String {
	let mut lines: Vec<String> = Vec::new();

	for idx in 0..cnt {
		let byte_idx = idx / 8;
		let bit_idx = idx % 8;

		let byte = data.get(byte_idx as usize).unwrap_or(&0);
		let coil = match byte & (1 << bit_idx) {
			0 => 0,
			_ => 1,
		};

		lines.push(format!("{}", coil));
	}

	lines.join(", ")
}

fn format_coils_hexlist(data: &[u8], _start_addr: u16, cnt: u16) -> String {
	let mut lines: Vec<String> = Vec::new();

	for idx in 0..cnt {
		let byte_idx = idx / 8;
		let bit_idx = idx % 8;

		let byte = data.get(byte_idx as usize).unwrap_or(&0);
		let coil = match byte & (1 << bit_idx) {
			0 => 0,
			_ => 1,
		};

		lines.push(format!("0x{:02X}", coil));
	}

	lines.join(", ")
}

fn byte_to_char(byte: u8) -> char {
	if byte < 32 || byte > 126 {
		return '.';
	}

	char::from_u32(byte as u32).unwrap_or('.')
}


fn format_registers_columns(data: &[u8], start_addr: u16, cnt: u16) -> String {
	let mut lines: Vec<String> = vec![
		"  addr | hex    |    BE |    LE | char".to_string()
	];

	for idx in 0..cnt {
		let address = start_addr + idx;
		let reg_idx = (idx * 2) as usize;

		let lsb = *data.get(reg_idx).unwrap_or(&255);
		let msb = *data.get(reg_idx + 1).unwrap_or(&255);
		let value_be = (lsb as u16) << 8 | msb as u16;
		let value_le = (msb as u16) << 8 | lsb as u16;

		lines.push(format!("{:6} | 0x{:04X} | {:5} | {:5} | {}{}",
			address, value_be, value_be, value_le,
			byte_to_char(lsb), byte_to_char(msb)));
	}

	lines.join("\n")
}

fn format_registers_json(data: &[u8], start_addr: u16, cnt: u16) -> String {
	let mut lines: Vec<String> = vec![];

	for idx in 0..cnt {
		let address = start_addr + idx;
		let reg_idx = (idx * 2) as usize;

		let lsb = *data.get(reg_idx).unwrap_or(&255) as u16;
		let msb = *data.get(reg_idx + 1).unwrap_or(&255) as u16;
		let value_be = lsb << 8 | msb;

		lines.push(format!("{{ \"address\": {}, \"value\": {} }}", address, value_be));
	}

	format!("[ {} ]", lines.join(", "))
}

fn format_registers_yml(data: &[u8], start_addr: u16, cnt: u16) -> String {
	let mut lines: Vec<String> = vec![];

	for idx in 0..cnt {
		let address = start_addr + idx;
		let reg_idx = (idx * 2) as usize;

		let lsb = *data.get(reg_idx).unwrap_or(&255) as u16;
		let msb = *data.get(reg_idx + 1).unwrap_or(&255) as u16;
		let value_be = lsb << 8 | msb;

		lines.push(format!("- address: {}\n  value: {}", address, value_be));
	}

	lines.join("\n")
}

fn format_registers_list(data: &[u8], _start_addr: u16, cnt: u16) -> String {
	let mut lines: Vec<String> = vec![];

	for idx in 0..cnt {
		let reg_idx = (idx * 2) as usize;

		let lsb = *data.get(reg_idx).unwrap_or(&255) as u16;
		let msb = *data.get(reg_idx + 1).unwrap_or(&255) as u16;
		let value_be = lsb << 8 | msb;
		lines.push(format!("{value_be}"));
	}

	lines.join(", ")
}

fn format_registers_hexlist(data: &[u8], _start_addr: u16, cnt: u16) -> String {
	let mut lines: Vec<String> = vec![];

	for idx in 0..cnt {
		let reg_idx = (idx * 2) as usize;

		let lsb = *data.get(reg_idx).unwrap_or(&0) as u16;
		let msb = *data.get(reg_idx + 1).unwrap_or(&0) as u16;
		let value_be = lsb << 8 | msb;
		lines.push(format!("0x{:04X}", value_be));
	}

	lines.join(", ")
}
