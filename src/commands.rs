use crate::connection::*;
use crate::modbus::*;
use crate::print::*;
use crate::errors::*;
use crate::context::Context;
use std::thread;
use std::time::Duration;

fn str_to_u16(s: &str) -> Result<u16, ()> {
	if s.starts_with("0x") {
		let cleaned_s = &s[2..];
		match u16::from_str_radix(cleaned_s, 16) {
			Ok(val) => Ok(val),
			_ => Err(()),
		}
	} else {
		match s.parse::<u16>() {
			Ok(val) => Ok(val),
			_ => Err(()),
		}
	}
}

fn str_to_bool(val: &str) -> bool {
	val != "0" && val.to_lowercase().as_str() != "false"
}

fn parse_read_parts(parts: &[&str]) -> Result<(u16, u16), String> {
	if parts.len() > 3 || parts.len() < 2 {
		let cmd = parts.get(0).unwrap_or(&"...");
		let msg = format!("{} command expects 1 or 2 arguments `{} ADDR [CNT = 1]`", cmd, cmd);
		return mk_input_error(msg.as_str());
	}

	let default_cnt = "1";

	let addr = match str_to_u16(parts.get(1).unwrap()) {
		Ok(val) => val,
		_ => return mk_input_error("invalid address format"),
	};

	let cnt = match str_to_u16(parts.get(2).unwrap_or(&default_cnt)) {
		Ok(val) => val,
		_ => return mk_input_error("invalid count format"),
	};

	if cnt == 0 {
		return mk_input_error("count must be greater than 0")
	}

	Ok((addr, cnt))
}

pub fn read_coils(parts: &[&str], client: &mut Context) -> Result<ModbusResult, String> {
	let (addr, cnt) = match parse_read_parts(parts) {
		Ok((a, b)) => (a, b),
		Err(err) => return Err(err),
	};

	let byte_vec = create_modbus_tcp_read_msg(0, 0, MODBUS_FN_CODE_READ_COILS, addr, cnt);
	client.connection.send(client.addr, &byte_vec.as_slice());
	let mut buffer = [0; 128];
	let bytes_read = client.connection.receive(&mut buffer);
	let result_bytes = &buffer[9..bytes_read];

	if is_modbus_error(*buffer.get(7).unwrap_or(&0xFF)) {
		let err_code = buffer.get(8).unwrap_or(&0);
		mk_modbus_error_result(*err_code)
	} else {
		println!("{}", format_coils(&client.fmt, result_bytes, addr, cnt));
		Ok(())
	}
}

fn is_modbus_error(fn_code: u8) -> bool {
	(fn_code & 0x80) != 0
}

fn mk_modbus_error_result(err_code: u8) -> Result<ModbusResult, String> {
	Err(fmt_error("ModbusError", match err_code {
		1 => "Illegal Function",
		2 => "Illegal Data Address",
		3 => "Illegal Data Value",
		4 => "Slave Device Failure",
		5 => "Acknowledge",
		6 => "Slave Device Busy",
		7 => "Negative Acknowledge",
		8 => "Memory Parity Error",
		10 => "Gateway Path Unavailable",
		11 => "Gateway Target Device Failed to Respond",
		_ => "Unknown",
	}))
}

pub fn read_discrete(parts: &[&str], client: &mut Context) -> Result<ModbusResult, String> {
	let (addr, cnt) = match parse_read_parts(parts) {
		Ok((a, b)) => (a, b),
		Err(err) => return Err(err),
	};

	let byte_vec = create_modbus_tcp_read_msg(0, 0, MODBUS_FN_CODE_READ_DISCRETE_INPUTS, addr, cnt);
	client.connection.send(client.addr, &byte_vec.as_slice());
	let mut buffer = [0; 128];
	let bytes_read = client.connection.receive(&mut buffer);
	let result_bytes = &buffer[9..bytes_read];

	if is_modbus_error(*buffer.get(7).unwrap_or(&0xFF)) {
		let err_code = buffer.get(8).unwrap_or(&0);
		mk_modbus_error_result(*err_code)
	} else {
		println!("{}", format_coils(&client.fmt, result_bytes, addr, cnt));
		Ok(())
	}
}

pub fn read_inputs(parts: &[&str], client: &mut Context) -> Result<ModbusResult, String> {
	let (addr, cnt) = match parse_read_parts(parts) {
		Ok((a, b)) => (a, b),
		Err(err) => return Err(err),
	};

	let byte_vec = create_modbus_tcp_read_msg(0, 0, MODBUS_FN_CODE_READ_INPUT_REGISTERS, addr, cnt);
	client.connection.send(client.addr, &byte_vec.as_slice());
	let mut buffer = [0; 128];
	let bytes_read = client.connection.receive(&mut buffer);
	let result_bytes = &buffer[9..bytes_read];

	if is_modbus_error(*buffer.get(7).unwrap_or(&0xFF)) {
		let err_code = buffer.get(8).unwrap_or(&0);
		mk_modbus_error_result(*err_code)
	} else {
		println!("{}", format_registers(&client.fmt, result_bytes, addr, cnt));
		Ok(())
	}
}

pub fn read_holding(parts: &[&str], client: &mut Context) -> Result<ModbusResult, String> {
	let (addr, cnt) = match parse_read_parts(parts) {
		Ok((a, b)) => (a, b),
		Err(err) => return Err(err),
	};

	let byte_vec = create_modbus_tcp_read_msg(0, 0, MODBUS_FN_CODE_READ_MULTIPLE_HOLDING_REGISTERS, addr, cnt);
	client.connection.send(client.addr, &byte_vec.as_slice());
	let mut buffer = [0; 128];
	let bytes_read = client.connection.receive(&mut buffer);
	let result_bytes = &buffer[9..bytes_read];

	if is_modbus_error(*buffer.get(7).unwrap_or(&0xFF)) {
		let err_code = buffer.get(8).unwrap_or(&0);
		mk_modbus_error_result(*err_code)
	} else {
		println!("{}", format_registers(&client.fmt, result_bytes, addr, cnt));
		Ok(())
	}
}

pub fn write_coils(parts: &[&str], client: &mut Context) -> Result<ModbusResult, String> {
	if parts.len() < 3 {
		return mk_input_error("wc command expects 2 or more arguments: `wc ADDR VAL1 [VAL2 ...]`");
	}

	let addr = match str_to_u16(parts.get(1).unwrap()) {
		Ok(val) => val,
		_ => return mk_input_error("invalid address format"),
	};

	let values_str = &parts[2..];
	let values: Vec<bool> = values_str.iter().map(|val| -> bool { str_to_bool(val) }).collect();

	let byte_vec = create_modbus_tcp_write_coils_msg(0, 0, addr, &values);
	client.connection.send(client.addr, &byte_vec.as_slice());
	let mut buffer = [0; 128];
	let _bytes_read = client.connection.receive(&mut buffer);

	if is_modbus_error(*buffer.get(7).unwrap_or(&0xFF)) {
		let err_code = buffer.get(8).unwrap_or(&0);
		mk_modbus_error_result(*err_code)
	} else {
		Ok(())
	}
}

pub fn write_single_coil(_parts: &[&str], _client: &mut Context) -> Result<ModbusResult, String> {
	mk_error("NotImplementedError", "write single coil not implemented yet")
}

pub fn write_holding_registers(parts: &[&str], client: &mut Context) -> Result<ModbusResult, String> {
	if parts.len() < 3 {
		return mk_input_error("wh command expects 2 or more arguments: `wh ADDR VAL1 [VAL2 ...]`");
	}

	let addr = match str_to_u16(parts.get(1).unwrap()) {
		Ok(val) => val,
		_ => return mk_input_error("invalid address format"),
	};

	let values_str = &parts[2..];
	let values_res_u16: Vec<Result<u16, ()>> = values_str.iter().map(|val| -> Result<u16, ()> { str_to_u16(val) }).collect();

	if values_res_u16.iter().any(|result| result.is_err()) {
		return mk_input_error("invalid register value provided");
	}

	let values: Vec<u16> = values_res_u16.iter().map(|val| -> u16 { val.unwrap() }).collect();

	let byte_vec = create_modbus_tcp_write_registers_msg(0, 0, addr, values.as_slice());
	client.connection.send(client.addr, &byte_vec.as_slice());
	let mut buffer = [0; 128];
	let _bytes_read = client.connection.receive(&mut buffer);

	if is_modbus_error(*buffer.get(7).unwrap_or(&0xFF)) {
		let err_code = buffer.get(8).unwrap_or(&0);
		mk_modbus_error_result(*err_code)
	} else {
		Ok(())
	}
}

pub fn write_single_holding_register(_parts: &[&str], _client: &Context) -> Result<ModbusResult, String> {
	mk_error("NotImplementedError", "write single holding register not implemented yet")
}

pub fn sleep(parts: &[&str]) -> Result<(), String> {
	if parts.len() > 2 {
		return mk_input_error("`sleep` command expects 0 or 1 argument: `sleep [DURATION_MS = 1]`");
	}

	let duration_str = parts.get(1).unwrap_or(&"1");
	let milliseconds = match duration_str.parse::<u64>() {
		Ok(val) => val,
		Err(_) => 1,
	};
	thread::sleep(Duration::from_millis(milliseconds));
	Ok(())
}

