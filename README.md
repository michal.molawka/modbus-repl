# modbus-repl

A small Modbus client REPL application.

## Command line arguments

| Arg | Description |
|---|---|
|--help           | displauy help text |
| --udp ADDR      | create ModbusUDP socket sending data to given address |
| --tcp ADDR      | connect to ModbusTCP server at given address |
| --rtu ADDR      | connect to ModbusRTU server at given address |
| --fmt FMT       | output format, supported: json, yml, columns, list, hex_list |
| --script PATH   | run a script from a file at given path |
| --exec COMMAND  | execute given command and exit |

## Example usage

```bash
./modbus-repl --udp 192.168.0.10:502
UDP/192.168.0.10:502 > rc 0 4
  addr | value
     0: 1
     1: 0
     2: 1
     3: 0
Ok
UDP/192.168.0.10:502 > ri 30020 2
  addr | hex    |    BE |    LE | char
 30020 | 0x0001 |     1 |   256 | ..
 30021 | 0x0A26 |  2598 |  9738 | .&
Ok
UDP/192.168.0.10:502 > wh 40000 0xABCD 65535
Ok
UDP/192.168.0.10:502 > rh 40000 2
  addr | hex    |    BE |    LE | char
 40000 | 0xABCD | 43981 | 52651 | ..
 40001 | 0xFFFF | 65535 | 65535 | ..
Ok
```
